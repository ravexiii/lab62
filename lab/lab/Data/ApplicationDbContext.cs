﻿using System;
using System.Collections.Generic;
using System.Text;
using lab.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace lab.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Cafe> Cafes { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Cafe>()
                .HasMany(d => d.Dishes)
                .WithOne(c => c.Cafes);


            modelBuilder.Entity<Dish>()
                .HasOne(c => c.Cafes)
                .WithMany(d => d.Dishes);
        }
    }
}
