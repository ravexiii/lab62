﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab.Models
{
    public class Dish : Entity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }

        public int? CafeId { get; set; }
        public Cafe Cafes { get; internal set; }
    }
}
