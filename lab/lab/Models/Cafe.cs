﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lab.Models
{
    public class Cafe : Entity
    {
        public string Name { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Avatar Image")]
        public IFormFile AvatarImage { get; set; }

        public string Avatar { get; set; }

        public string Description { get; set; }

        public IEnumerable<Dish> Dishes { get; set; }
    }
}
